﻿/// <summary>
/// Map display mode
/// </summary>
public enum DisplayMode
{
    /// <summary>
    /// Normal display mode
    /// </summary>
    Normal,

    /// <summary>
    /// Temperature display mode
    /// </summary>
    Temperature,

    /// <summary>
    /// Moisture display mode
    /// </summary>
    Moisture
}