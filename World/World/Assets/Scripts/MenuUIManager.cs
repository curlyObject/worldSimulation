﻿using UnityEngine;

/// <summary>
/// Handles user input
/// </summary>
public class MenuUIManager : MonoBehaviour
{
    /// <summary>
    /// Exits the game
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Loads a Unity scene
    /// </summary>
    /// <param name="level">Unity level to load</param>
    public void LoadLevel(string level)
    {
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(level);
    }
}
