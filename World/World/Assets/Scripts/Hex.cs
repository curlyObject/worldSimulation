﻿using UnityEngine;
using World.Framework.Terrain;

/// <summary>
/// Unity hex script
/// </summary>
public class Hex : MonoBehaviour
{
    /// <summary>
    /// Gets the tile represented by this hex
    /// </summary>
    public Tile Tile { get; private set; }

    /// <summary>
    /// Gets or sets the unit present on this hex
    /// </summary>
    public Unit Unit { get; set; }

    /// <summary>
    /// Gets the height of the hex game object
    /// </summary>
    public float MeshHeight
    {
        get
        {
            return this.GetComponentInChildren<Renderer>().bounds.extents.y;
        }
    }

    /// <summary>
    /// Initializes the hex with a tile
    /// </summary>
    /// <param name="tile">Tile represented by this hex object</param>
    public void Initialize(Tile tile)
    {
        this.Tile = tile;
        this.SetColor();
        this.SetHeight();
    }

    /// <summary>
    /// Switches to the specified visualization mode
    /// </summary>
    /// <param name="mode">Mode to switch to</param>
    public void SwitchVisualisation(DisplayMode mode)
    {
        switch (mode)
        {
            case DisplayMode.Moisture:
                this.SetColor(new Color(this.Tile.Temperature, 0, 0));
                break;
            case DisplayMode.Temperature:
                this.SetColor(new Color(0, 0, this.Tile.Moisture));
                break;
            default:
                this.SetColor();
                break;
        }
    }

    /// <summary>
    /// Sets model color
    /// </summary>
    /// <param name="color">Unity color object to set</param>
    private void SetColor(Color color)
    {
        this.GetComponentInChildren<MeshRenderer>().material.color = color;
    }

    /// <summary>
    /// Sets scale of height relative to tile elevation
    /// </summary>
    private void SetHeight()
    {
        var renderer = this.GetComponent<RectTransform>();

        // 5 is arbitrary
        renderer.transform.localScale += new Vector3(0, this.Tile.Elevation * 5f, 0);
        renderer.transform.position += new Vector3(0, this.MeshHeight, 0);
    }

    /// <summary>
    /// Sets color relative to tile terrain type
    /// </summary>
    private void SetColor()
    {
        if (this.Tile.River)
        {
            this.SetColor(new Color(0.2f, 0.6f, 0.8f));
            return;
        }

        switch (this.Tile.Type)
        {
            case TerrainType.Ocean:
                this.SetColor(Color.blue);
                break;
            case TerrainType.Plain:
                this.SetColor(Color.green);
                break;
            case TerrainType.Desert:
                this.SetColor(Color.yellow);
                break;
            case TerrainType.Dryland:
                this.SetColor(new Color(0.6f, 0.45f, 0.33f));
                break;
            case TerrainType.Jungle:
                this.SetColor(new Color(0, 0.5f, 0));
                break;
            case TerrainType.Woodland:
                this.SetColor(new Color(0.26f, 0.32f, 0.15f));
                break;
            case TerrainType.Artic:
                this.SetColor(Color.white);
                break;
            case TerrainType.Mountain:
                this.SetColor(Color.grey);
                break;
        }
    }
}