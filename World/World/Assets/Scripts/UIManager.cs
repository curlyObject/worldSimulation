﻿using UnityEngine;

/// <summary>
/// Handles user input
/// </summary>
public class UIManager : MonoBehaviour
{
    /// <summary>
    /// Objects that appear when the game is paused
    /// </summary>
    private GameObject[] pauseObjects;
    
    /// <summary>
    /// Use this for initialization
    /// </summary>
    public void Start()
    {
        Time.timeScale = 1;
        this.pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        this.HidePaused();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    public void Update()
    {
        if (hardInput.GetKeyDown(Command.Pause))
        {
            this.TogglePaused();
        }
    }

    /// <summary>
    /// Toggles the pausing of the game
    /// </summary>
    public void OnResumeClick()
    {
        this.TogglePaused();
    }

    /// <summary>
    /// Exits the game
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }

    /// <summary>
    ///  Shows objects with ShowOnPause tag
    /// </summary>
    public void ShowPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }

    /// <summary>
    /// Hides objects with ShowOnPause tag
    /// </summary>
    public void HidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    /// <summary>
    /// Loads a Unity scene
    /// </summary>
    /// <param name="level">Unity level to load</param>
    public void LoadLevel(string level)
    {
        this.TogglePaused();
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(level);
    }

    /// <summary>
    /// Toggles whether the game is paused
    /// </summary>
    private void TogglePaused()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            this.ShowPaused();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            this.HidePaused();
        }
    }
}
