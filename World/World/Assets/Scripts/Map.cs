﻿using System.Collections.Generic;
using UnityEngine;
using World.Framework.Terrain;
using World.Framework.WorldGen;

/// <summary>
/// Unity object which represents the game map
/// </summary>
public class Map : MonoBehaviour
{
    /// <summary>
    /// Prefab which will be instantiated for each hex
    /// </summary>
    public GameObject HexPrefab;

    /// <summary>
    /// Prefab which will be instantiated for each unit
    /// </summary>
    public GameObject UnitPrefab;

    /// <summary>
    /// Prefab which will be instantiated for each hex text
    /// </summary>
    public GameObject CoordTextPrefab;

    /// <summary>
    /// Unity camera object
    /// </summary>
    public UnityEngine.Camera Camera;

    /// <summary>
    /// List of instantiated hexes
    /// </summary>
    private Dictionary<HexCoord, GameObject> hexes;

    /// <summary>
    /// List of instantiated units
    /// </summary>
    private List<GameObject> units;

    /// <summary>
    /// List of instantiated units
    /// </summary>
    private GameObject selected;

    /// <summary>
    /// List of instantiates coordinate labels
    /// </summary>
    private List<GameObject> coordTexts;

    /// <summary>
    /// Display mode of the hex
    /// </summary>
    private DisplayMode mode;

    /// <summary>
    /// Unity start function called at object instantiation
    /// </summary>
    public void Start()
    {
        this.Create(MapOptions.DefaultRegionalMap());
        this.CreateUnit(new HexCoord(1, 1));
        this.CreateUnit(new HexCoord(2, 2));
        this.CreateUnit(new HexCoord(1, 3));
        this.CreateUnit(new HexCoord(2, 4));
        this.CreateUnit(new HexCoord(1, 5));
    }

    /// <summary>
    /// Unity update function called once per frame
    /// </summary>
    public void Update()
    {
        // Regenrate regional map
        if (hardInput.GetKeyDown(Command.RegenerateRegional))
        {
            this.Destroy();
            this.Create(MapOptions.DefaultRegionalMap());
        }

        // Regenerate world map
        if (hardInput.GetKeyDown(Command.RegenerateWorld))
        {
            this.Destroy();
            this.Create(MapOptions.DefaultWorldMap());
        }

        // Toggle hex coordinates
        if (hardInput.GetKeyDown(Command.DisplayCoordinates))
        {
            if (this.coordTexts == null)
            {
                this.DisplayCoordinates();
            }
            else
            {
                this.HideCoordinates();
            }
        }

        if (hardInput.GetKeyDown(Command.ToggleVisualisation))
        {
            this.ToggleVisualisation();
        }

        GameObject hit = null;
        if (Mouse.IsLeftClickOn(this.Camera, "Unit", out hit))
        {
            if (this.selected)
            {
                this.selected.GetComponent<Unit>().DeSelect();
            }

            this.selected = hit;
            this.selected.GetComponent<Unit>().Select();
        }

        if (Mouse.IsRightClickOn(this.Camera, "Hex", out hit))
        {
            var hex = hit.gameObject.GetComponent<Hex>();
            if (hex.Tile.IsTraversable && hex.Unit == null)
            {
                this.selected.GetComponent<Unit>().Hex.Unit = null;
                this.selected.GetComponent<RectTransform>().transform.position = new Vector3(hex.Tile.Coord.X, 0, hex.Tile.Coord.Z);
                this.selected.GetComponent<Unit>().Initialize(hex);
                hex.Unit = this.selected.GetComponent<Unit>();
            }
        }
    }

    /// <summary>
    /// Generates a map
    /// </summary>
    /// <param name="options">Map options</param>
    private void Create(MapOptions options)
    {
        var map = MapBuilder.BuildRectangularMap(101, 101, options);
        this.hexes = new Dictionary<HexCoord, GameObject>();

        foreach (var tile in map.Tiles)
        {
            var x = tile.Value.Coord.X;
            var z = tile.Value.Coord.Z;

            var hex = Instantiate(this.HexPrefab, new Vector3(x, 0, (float)z), Quaternion.identity);
            this.hexes.Add(tile.Key, hex);

            hex.GetComponent<Hex>().Initialize(tile.Value);
        }
    }

    /// <summary>
    /// Generates a unit
    /// </summary>
    /// <param name="coord">Hex coordinates to place unit on</param>
    private void CreateUnit(HexCoord coord)
    {
        this.units = new List<GameObject>();

        var hex = this.hexes[coord].GetComponent<Hex>();
        var tile = hex.Tile;

        var unit = Instantiate(this.UnitPrefab, new Vector3(coord.X, 0, coord.Z), Quaternion.identity);
        this.units.Add(unit);

        unit.GetComponent<Unit>().Initialize(hex);
        hex.Unit = unit.GetComponent<Unit>();
    }

    /// <summary>
    /// Destroys generated hexes
    /// </summary>
    private void Destroy()
    {
        foreach (var hex in this.hexes)
        {
            Object.Destroy(hex.Value);
        }

        this.hexes = null;
    }

    /// <summary>
    /// Instantiates a text mesh for each tile, displaying it's axial coordinate
    /// </summary>
    private void DisplayCoordinates()
    {
        this.coordTexts = new List<GameObject>();
        foreach (var hex in this.hexes.Values)
        {
            var innerHex = hex.GetComponent<Hex>();
            var position = new Vector3(hex.transform.position.x, innerHex.MeshHeight, hex.transform.position.z);
            var text = Instantiate(this.CoordTextPrefab, position, Quaternion.identity);
            text.GetComponent<TextMesh>().text = "o:" + innerHex.Tile.Coord.OffsetQ + "," + innerHex.Tile.Coord.OffsetR 
                + "\na:" + innerHex.Tile.Coord.Q + "," + innerHex.Tile.Coord.R;
            this.coordTexts.Add(text);
        }
    }

    /// <summary>
    /// Destroys coordinate text objects
    /// </summary>
    private void HideCoordinates()
    {
        foreach (var coord in this.coordTexts)
        {
            Object.Destroy(coord);
        }

        this.coordTexts = null;
    }

    /// <summary>
    /// Toggles map visualizations
    /// </summary>
    private void ToggleVisualisation()
    {
        switch (this.mode)
        {
            case DisplayMode.Moisture:
                this.mode = DisplayMode.Temperature;
                break;
            case DisplayMode.Temperature:
                this.mode = DisplayMode.Normal;
                break;
            default:
                this.mode = DisplayMode.Moisture;
                break;
        }

        this.SetVisualisation(this.mode);
    }

    /// <summary>
    /// Toggles map visualizations
    /// </summary>
    /// <param name="mode">Display mode to switch to</param>
    private void SetVisualisation(DisplayMode mode)
    {
        foreach (var hex in this.hexes.Values)
        {
            var innerHex = hex.GetComponent<Hex>();
            innerHex.SwitchVisualisation(mode);
        }
    }
}
