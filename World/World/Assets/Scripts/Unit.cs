﻿using UnityEngine;
using World.Framework.Terrain;

/// <summary>
/// Unity unit script
/// </summary>
public class Unit : MonoBehaviour
{
    /// <summary>
    /// Gets the hex this unit is on
    /// </summary>
    public Hex Hex { get; private set; }

    /// <summary>
    /// Gets the height of the hex game object
    /// </summary>
    public float MeshHeight
    {
        get
        {
            return this.GetComponentInChildren<Renderer>().bounds.extents.y;
        }
    }

    /// <summary>
    /// Gets or sets the color of the unit
    /// </summary>
    private Color Color { get; set; }

    /// <summary>
    /// Sets unit color at object creation
    /// </summary>
    public void Start()
    {
        this.Color = this.GetComponentInChildren<MeshRenderer>().material.color;
    }

    /// <summary>
    /// Initializes the unit with a tile
    /// </summary>
    /// <param name="hex">Hex represented by this hex object</param>
    public void Initialize(Hex hex)
    {
        this.Hex = hex;
        this.SetPosition();
    }

    /// <summary>
    /// Selects unit
    /// </summary>
    public void Select()
    {
        this.GetComponentInChildren<MeshRenderer>().material.color = Color.magenta;
    }

    /// <summary>
    /// De-selects unit
    /// </summary>
    public void DeSelect()
    {
        this.GetComponentInChildren<MeshRenderer>().material.color = this.Color;
    }

    /// <summary>
    /// Sets model color
    /// </summary>
    /// <param name="color">Unity color object to set</param>
    private void SetColor(Color color)
    {
        this.Color = color;
        this.GetComponentInChildren<MeshRenderer>().material.color = color;
    }

    /// <summary>
    /// Sets position relative to tile height
    /// </summary>
    private void SetPosition()
    {
        var renderer = this.GetComponent<RectTransform>();
        var pos = renderer.transform.position;
        renderer.transform.position += new Vector3(0, (this.Hex.MeshHeight * 2) + this.MeshHeight, 0);
    }
}