﻿using UnityEngine;

/// <summary>
/// Camera controls the player camera
/// </summary>
public class Camera : MonoBehaviour
{
    /// <summary>
    /// Camera move speed
    /// </summary>
    private const int Speed = 30;

    /// <summary>
    /// Maximum zoom level
    /// </summary>
    private const float MaxZoom = 5f;

    /// <summary>
    /// Unity update function called once per frame
    /// </summary>
    public void Update()
    {
        float x = transform.position.x;
        x += WithSpeedAndDelta(hardInput.GetAxis(Command.CameraRight, Command.CameraLeft, 1));

        float z = transform.position.z;
        z += WithSpeedAndDelta(hardInput.GetAxis(Command.CameraForward, Command.CameraBack, 1));

        float y = transform.position.y;

        // Zoom in
        if (hardInput.GetKey(Command.CameraZoomIn))
        {
            y += WithSpeedAndDelta(-1f);
        }

        // Zoom out
        if (hardInput.GetKey(Command.CameraZoomOut))
        {
            y += WithSpeedAndDelta(1f);
        }

        y = Mathf.Max(MaxZoom, y);
        transform.position = new Vector3(x, y, z);
    }

    /// <summary>
    /// Adjusts movement value by speed and delta time
    /// </summary>
    /// <param name="value">Value to modify</param>
    /// <returns>Adjusted movement value</returns>
    private static float WithSpeedAndDelta(float value)
    {
        return value * Speed * Time.deltaTime;
    }
}
