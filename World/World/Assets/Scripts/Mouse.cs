﻿using UnityEngine;

/// <summary>
/// Mouse related helper functions
/// </summary>
public static class Mouse
{
    /// <summary>
    /// Determines if any object with a specific tag was clicked
    /// </summary>
    /// <param name="camera">Unity camera instance</param>
    /// <param name="button">Button to check</param>
    /// <param name="tag">Tag to check for</param>
    /// <param name="target">Target object</param>
    /// <returns>True if object was clicked</returns>
    public static bool IsClickOn(UnityEngine.Camera camera, int button, string tag, out GameObject target)
    {
        if (Input.GetMouseButtonDown(button))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hitInfo);

            if (hit && hitInfo.transform.parent.gameObject.tag == tag)
            {
                target = hitInfo.transform.parent.gameObject;
                return true;
            }
        }

        target = null;
        return false;
    }

    /// <summary>
    /// Determines if any object with a specific tag was clicked
    /// </summary>
    /// <param name="camera">Unity camera instance</param>
    /// <param name="tag">Tag to check for</param>
    /// <param name="target">Target object</param>
    /// <returns>True if object was clicked</returns>
    public static bool IsLeftClickOn(UnityEngine.Camera camera, string tag, out GameObject target)
    {
        return IsClickOn(camera, 0, tag, out target);
    }

    /// <summary>
    /// Determines if any object with a specific tag was clicked
    /// </summary>
    /// <param name="camera">Unity camera instance</param>
    /// <param name="tag">Tag to check for</param>
    /// <param name="target">Target object</param>
    /// <returns>True if object was clicked</returns>
    public static bool IsRightClickOn(UnityEngine.Camera camera, string tag, out GameObject target)
    {
        return IsClickOn(camera, 1, tag, out target);
    }
}