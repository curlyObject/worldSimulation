﻿/// <summary>
/// Commands used for key bindings
/// </summary>
public static class Command
{
    /// <summary>
    /// Moves the camera forward
    /// </summary>
    public const string CameraForward = "Forward";

    /// <summary>
    /// Moves the camera back
    /// </summary>
    public const string CameraBack = "Back";

    /// <summary>
    /// Moves the camera left
    /// </summary>
    public const string CameraLeft = "Left";

    /// <summary>
    /// Moves the camera right
    /// </summary>
    public const string CameraRight = "Right";

    /// <summary>
    /// Zoom in the camera
    /// </summary>
    public const string CameraZoomIn = "Zoom In";

    /// <summary>
    /// Zoom out the camera
    /// </summary>
    public const string CameraZoomOut = "Zoom Out";

    /// <summary>
    /// Toggles pause menu
    /// </summary>
    public const string Pause = "Pause";

    /// <summary>
    /// Toggles visualization modes
    /// </summary>
    public const string ToggleVisualisation = "Toggle Visualisation";

    /// <summary>
    /// Regenerates the map
    /// </summary>
    public const string RegenerateRegional = "Regenerate Regional";

    /// <summary>
    /// Regenerates the map
    /// </summary>
    public const string RegenerateWorld = "Regenerate World";

    /// <summary>
    /// Toggles display of coordinates on the map
    /// </summary>
    public const string DisplayCoordinates = "Show Coordinates";
}