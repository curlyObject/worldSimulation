﻿namespace World.Framework.WorldGen
{
    /// <summary>
    /// Interface for noise generators
    /// </summary>
    public interface INoiseMap
    {
        /// <summary>
        /// Gets or sets noise seed
        /// </summary>
        int Seed { get; set; }

        /// <summary>
        /// Gets or sets map width
        /// </summary>
        int Width { get; set; }

        /// <summary>
        /// Gets or sets map height
        /// </summary>
        int Height { get; set; }

        /// <summary>
        /// Gets or sets number of octaves
        /// </summary>
        int Octaves { get; set; }

        /// <summary>
        /// Gets or sets noise frequency
        /// </summary>
        float Frequency { get; set; }

        /// <summary>
        /// Get value returns a noise value for pair of coordinates
        /// </summary>
        /// <param name="x">X Coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <returns>Noise value</returns>
        float GetValue(float x, float y);
    }
}
