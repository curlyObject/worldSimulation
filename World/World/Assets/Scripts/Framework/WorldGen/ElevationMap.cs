﻿namespace World.Framework.WorldGen
{
    using UnityEngine;
    using World.Framework.Terrain;

    /// <summary>
    /// Elevation map
    /// </summary>
    public class ElevationMap
    {
        /// <summary>
        /// Noise generator
        /// </summary>
        private INoiseMap noiseMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="ElevationMap"/> class.
        /// </summary>
        /// <param name="noiseMap">Noise creator</param>
        public ElevationMap(INoiseMap noiseMap)
        {
            this.noiseMap = noiseMap;
            this.noiseMap.Frequency = 4f;
            this.noiseMap.Octaves = 3;
        }

        /// <summary>
        /// Get elevation value for coordinate
        /// </summary>
        /// <param name="coord">Coordinate to get elevation for</param>
        /// <returns>Elevation value in the approximate range of -1.0 to 1.0</returns>
        public float GetElevation(HexCoord coord)
        {
            var elevation = this.noiseMap.GetValue((float)coord.Q, (float)coord.R);
            return Mathf.Pow(elevation, 2f);
        }
    }
}
