﻿namespace World.Framework.WorldGen
{
    using World.Framework.Terrain;

    /// <summary>
    /// Map creation options
    /// </summary>
    public class MapOptions
    {
        /// <summary>
        /// Default water elevation level
        /// </summary>
        private const float DefaultWaterLevel = 0.05f;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapOptions"/> class.
        /// </summary>
        public MapOptions()
        {
            this.WaterLevel = DefaultWaterLevel;
        }

        /// <summary>
        /// Gets or sets terrain type to use for hot and wet areas
        /// </summary>
        public TerrainType HotAndWet { get; set; }

        /// <summary>
        /// Gets or sets terrain type to use for hot and dry areas
        /// </summary>
        public TerrainType HotAndDry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the map should include arctic terrain
        /// </summary>
        public bool Artic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the map should include wood terrain
        /// </summary>
        public float WaterLevel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the map should cold poles
        /// </summary>
        public bool PolarTemperatures { get; set; }

        /// <summary>
        /// Returns default options for a world-scale map
        /// </summary>
        /// <returns>Map options</returns>
        public static MapOptions DefaultWorldMap()
        {
            return new MapOptions
            {
                HotAndWet = TerrainType.Jungle,
                HotAndDry = TerrainType.Desert,
                Artic = true,
                PolarTemperatures = true,
                WaterLevel = DefaultWaterLevel
            };
        }

        /// <summary>
        /// Returns default options for a regional-scale map
        /// </summary>
        /// <returns>Map options</returns>
        public static MapOptions DefaultRegionalMap()
        {
            return new MapOptions
            {
                HotAndWet = TerrainType.Woodland,
                HotAndDry = TerrainType.Dryland,
                Artic = false,
                PolarTemperatures = false,
                WaterLevel = DefaultWaterLevel
            };
        }
    }
}
