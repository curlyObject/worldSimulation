﻿namespace World.Framework.WorldGen
{
    using System;
    using System.Linq;
    using World.Framework.Terrain;

    /// <summary>
    /// Static map building functions
    /// </summary>
    public class MapBuilder
    {
        /// <summary>
        /// Generates an 'even-q' rectangular map of tiles using axial coordinates.
        /// Tiles are given a random terrain type.
        /// </summary>
        /// <param name="width">Width of map</param>
        /// <param name="height">Height of map</param>
        /// <param name="options">Map generation options</param>
        /// <returns>Map of specified size</returns>
        public static Map BuildRectangularMap(int width, int height, MapOptions options)
        {
            var map = new Map(width, height)
            {
                WaterLevel = options.WaterLevel
            };
            var elevationMap = new ElevationMap(new PerlinNoiseMap(width, height));
            var temperatureMap = new TemperatureMap(new PerlinNoiseMap(width, height), options.PolarTemperatures);
            var moistureMap = new MoistureMap(new PerlinNoiseMap(width, height));

            for (int q = 0; q < width; q++)
            {
                int q_offset = (int)Math.Floor((float)q / 2f);
                for (int r = -q_offset; r < height - q_offset; r++)
                {
                    var coord = new HexCoord(q, r);
                    var elevation = elevationMap.GetElevation(coord);
                    var temperature = temperatureMap.GetTemperature(coord, elevation);
                    var moisture = moistureMap.GetMoisture(coord);

                    var tile = new Tile(coord)
                    {
                        Elevation = elevation,
                        Temperature = temperature,
                        Moisture = moisture
                    };

                    tile.Type = GetTerrainType(tile, options);
                    map.Tiles.Add(coord, tile);
                }
            }

            AddRivers(map);

            return map;
        }

        /// <summary>
        /// Determines terrain type using tile values
        /// </summary>
        /// <param name="tile">Tile to get terrain type for</param>
        /// <param name="options">Map generation options</param>
        /// <returns>Terrain type</returns>
        public static TerrainType GetTerrainType(Tile tile, MapOptions options)
        {
            if (tile.Elevation > 0.8)
            {
                return TerrainType.Mountain;
            }

            if (tile.Elevation < options.WaterLevel)
            {
                // Water is flat, so set the tile elevation to the water level
                tile.Elevation = options.WaterLevel;
                return TerrainType.Ocean;
            }

            if (tile.Temperature > 0.5 && tile.Moisture > 0.6)
            {
                return options.HotAndWet;
            }

            if (tile.Temperature > 0.5 && tile.Moisture < 0.4)
            {
                return options.HotAndDry;
            }

            if (options.Artic && tile.Temperature < 0.1)
            {
                return TerrainType.Artic;
            }

            return TerrainType.Plain;
        }

        /// <summary>
        /// Generates rivers and adds them to an existing map
        /// </summary>
        /// <param name="map">Map to add rivers to</param>
        private static void AddRivers(Map map)
        {
            // Take 5% of tiles as potential river starts
            var potentials = (map.Height * map.Width) / 20;

            // Pick 0.5% of tiles as river starts
            var final = (map.Height * map.Width) / 200;

            // The general idea is to take a selection of the highest, wettest tiles
            // Then pick a random selection of those as the final river starts
            var riverStarts = map.Tiles.OrderByDescending(x => x.Value.Moisture + x.Value.Elevation)
                .Take(potentials)
                .OrderBy(x => Guid.NewGuid())
                .Take(final);

            foreach (var x in riverStarts)
            {
                x.Value.River = true;
                FindRiver(map, x.Value);
            }
        }

        /// <summary>
        /// Searches for a path for a river
        /// </summary>
        /// <param name="map">Map to find river in</param>
        /// <param name="current">Current river tile</param>
        private static void FindRiver(Map map, Tile current)
        {
            // The potential next river tile is a random neighbor that has a lower elevation than the current tile
            var next = map.GetNeighbors(current)
                .Where(x => x.Elevation < current.Elevation)
                .OrderBy(x => Guid.NewGuid())
                .FirstOrDefault();

            // To proceed the next tile must be not a river already and not an ocean
            if (next != null && next.Type != TerrainType.Ocean && !next.River)
            {
                FindRiver(map, next);
                next.River = true;
            }
            else
            {
                // Otherwise set the current tile to an ocean and call it a day
                current.Type = TerrainType.Ocean;
                current.River = false;
            }
        }
    }
}