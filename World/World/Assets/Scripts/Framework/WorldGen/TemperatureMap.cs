﻿namespace World.Framework.WorldGen
{
    using System;
    using UnityEngine;
    using World.Framework.Terrain;

    /// <summary>
    /// Temperature map
    /// </summary>
    public class TemperatureMap
    {
        /// <summary>
        /// Noise generator
        /// </summary>
        private INoiseMap noiseMap;

        /// <summary>
        /// Equator of map
        /// </summary>
        private int equator;

        /// <summary>
        /// Whether temperature should be colder further from the equator
        /// </summary>
        private bool poles;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemperatureMap"/> class.
        /// </summary>
        /// <param name="noiseMap">Noise creator</param>
        /// <param name="poles">Whether to adjust temperatures based on distance from equator</param>
        public TemperatureMap(INoiseMap noiseMap, bool poles)
        {
            this.noiseMap = noiseMap;
            this.noiseMap.Frequency = 1.5f;
            this.noiseMap.Octaves = 3;
            this.equator = noiseMap.Height / 2;
            this.poles = poles;
        }

        /// <summary>
        /// Get temperature value for coordinate
        /// </summary>
        /// <param name="coord">Coordinate to get temperature for</param>
        /// <param name="elevation">Elevation of coordinate</param>
        /// <returns>Temperature value in the approximate range of -1.0 to 1.0</returns>
        public float GetTemperature(HexCoord coord, float elevation)
        {
            var adjustedElevation = (float)Math.Pow(Math.Max(elevation, 0.1f), 6);

            var noise = this.noiseMap.GetValue(coord.Q, coord.R);
            var adjustedNoise = noise - adjustedElevation;

            if (this.poles)
            {
                var distance = (float)Math.Abs(this.equator - coord.OffsetR);
                var adjustedDistance = (float)Math.Pow(distance / this.equator, 8);
                adjustedNoise -= adjustedDistance;
            }

            return adjustedNoise;
        }
    }
}
