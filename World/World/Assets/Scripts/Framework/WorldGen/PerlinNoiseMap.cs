﻿namespace World.Framework.WorldGen
{
    using System;
    using UnityEngine;

    /// <summary>
    /// Noise map
    /// </summary>
    public class PerlinNoiseMap : INoiseMap
    {
        /// <summary>
        /// Static random number generator for seed generation
        /// </summary>
        private static System.Random rand = new System.Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="PerlinNoiseMap"/> class.
        /// </summary>
        /// <param name="width">Map width</param>
        /// <param name="height">Map height</param>
        public PerlinNoiseMap(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.Frequency = 1;
            this.Octaves = 1;
            this.Seed = rand.Next(999999);
        }

        /// <summary>
        /// Gets or sets the width of the map
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the map
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the frequency of the noise
        /// </summary>
        public float Frequency { get; set; }

        /// <summary>
        /// Gets or sets the noise seed
        /// </summary>
        public int Seed { get; set; }

        /// <summary>
        /// Gets or sets the number of octaves of noise
        /// </summary>
        public int Octaves { get; set; }

        /// <summary>
        /// Get noise value for coordinate
        /// </summary>
        /// <param name="x">X coordinate to get elevation for</param>
        /// <param name="y">Y coordinate to get elevation for</param>
        /// <returns>Noise value in the approximate range of -1.0 to 1.0</returns>
        public float GetValue(float x, float y)
        {
            var nq = x / (float)this.Width;
            var nr = y / (float)this.Height;
            var noise = 0.0f;
            var freq = this.Frequency;

            for (int o = 1; o <= this.Octaves; o++)
            {
                noise += (1.0f / (float)o) * this.GetNoise(nq, nr, freq);
                freq *= 2;
            }

            return Math.Max(noise + 0.5f, 0.0f);
        }

        /// <summary>
        /// Returns a noise value in the vague range -1.0 to 1.0
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="freq">Noise frequency</param>
        /// <returns>Noise value</returns>
        private float GetNoise(float x, float y, float freq)
        {
            return Mathf.PerlinNoise((float)this.Seed + (x * freq), (float)this.Seed + (y * freq)) - 0.5f;
        }
    }
}
