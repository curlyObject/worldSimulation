﻿namespace World.Framework.WorldGen
{
    using UnityEngine;
    using World.Framework.Terrain;

    /// <summary>
    /// Moisture map
    /// </summary>
    public class MoistureMap
    {
        /// <summary>
        /// Noise generator
        /// </summary>
        private INoiseMap noiseMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoistureMap"/> class.
        /// </summary>
        /// <param name="noiseMap">Noise creator</param>
        public MoistureMap(INoiseMap noiseMap)
        {
            this.noiseMap = noiseMap;
            this.noiseMap.Frequency = 4f;
            this.noiseMap.Octaves = 3;
        }

        /// <summary>
        /// Get moisture value for coordinate
        /// </summary>
        /// <param name="coord">Coordinate to get moisture for</param>
        /// <returns>Moisture value in the approximate range of -1.0 to 1.0</returns>
        public float GetMoisture(HexCoord coord)
        {
            return this.noiseMap.GetValue((float)coord.Q, (float)coord.R);
        }
    }
}
