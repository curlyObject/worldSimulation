﻿namespace World.Framework.Terrain
{
    using UnityEngine;

    /// <summary>
    /// Hex coordinates
    /// </summary>
    public class HexCoord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HexCoord"/> class.
        /// </summary>
        /// <param name="q">Q axial coordinate</param>
        /// <param name="r">R axial coordinate</param>
        public HexCoord(int q, int r)
        {
            this.Q = q;
            this.R = r;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HexCoord"/> class/
        /// </summary>
        /// <param name="x">X pixel coordinate</param>
        /// <param name="z">Z pixel coordinate</param>
        public HexCoord(float x, float z)
        {
            var q = x * 2f / 3f;
            var r = (-x / 3) + (Mathf.Sqrt(3) / 3 * z);

            // TODO: This rounding isn't perfect
            this.Q = (int)Mathf.Round(q);
            this.R = (int)Mathf.Round(r);
        }

        /// <summary>
        /// Gets Q axial coordinate
        /// </summary>
        public int Q { get; private set; }

        /// <summary>
        /// Gets R axial coordinate
        /// </summary>
        public int R { get; private set; }

        /// <summary>
        /// Gets Q axial coordinate converted to offset coordinate
        /// </summary>
        public int OffsetQ
        {
            get
            {
                return this.Q;
            }
        }

        /// <summary>
        /// Gets R axial coordinate converted to offset coordinate
        /// </summary>
        public int OffsetR
        {
            get
            {
                return this.R + (int)((this.Q + (this.Q & 1)) / 2f);
            }
        }

        /// <summary>
        /// Gets pixel X value
        /// </summary>
        public float X
        {
            get
            {
                return 3f / 2f * this.Q;
            }
        }

        /// <summary>
        /// Gets pixel Z value
        /// </summary>
        public float Z
        {
            get
            {
                return Mathf.Sqrt(3) * (this.R + (this.Q / 2f));
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int hash = 17;
            hash = (hash * 23) + this.Q.GetHashCode();
            hash = (hash * 29) + this.R.GetHashCode();
            return hash;
        }

        /// <inheritdoc/>
        public override bool Equals(object that)
        {
            return this.Equals(that as HexCoord);
        }

        /// <summary>
        /// Returns true if this hex coordinate is equal to another
        /// </summary>
        /// <param name="that">Hex coordinate to compare</param>
        /// <returns>True if hex coordinates are equal</returns>
        public bool Equals(HexCoord that)
        {
            return this.Q == that.Q && this.R == that.R;
        }
    }
}
