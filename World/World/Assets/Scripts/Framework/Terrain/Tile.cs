﻿namespace World.Framework.Terrain
{
    /// <summary>
    /// Hex tile
    /// </summary>
    public class Tile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tile"/> class.
        /// </summary>
        /// <param name="coord">Tile coordinate</param>
        public Tile(HexCoord coord)
        {
            this.Coord = coord;
        }

        /// <summary>
        /// Gets tile coordinates
        /// </summary>
        public HexCoord Coord { get; private set; }

        /// <summary>
        /// Gets or sets terrain type
        /// </summary>
        public TerrainType Type { get; set; }

        /// <summary>
        /// Gets or sets tile elevation
        /// </summary>
        public float Elevation { get; set; }

        /// <summary>
        /// Gets or sets tile temperature
        /// </summary>
        public float Temperature { get; set; }

        /// <summary>
        /// Gets or sets tile moisture
        /// </summary>
        public float Moisture { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there is a river crossing this tile
        /// </summary>
        public bool River { get; set; }

        /// <summary>
        /// Gets a value indicating whether this tile is traversable
        /// </summary>
        public bool IsTraversable
        {
            get
            {
                return this.Type != TerrainType.Ocean
                    && this.Type != TerrainType.Mountain;
            }
        }
    }
}
