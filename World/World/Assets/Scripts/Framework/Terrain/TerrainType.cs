﻿namespace World.Framework.Terrain
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// TerrainType enumeration
    /// </summary>
    public enum TerrainType
    {
        /// <summary>
        /// Plains terrain
        /// </summary>
        Plain,

        /// <summary>
        /// Mountain terrain
        /// </summary>
        Mountain,

        /// <summary>
        /// Ocean terrain
        /// </summary>
        Ocean,

        /// <summary>
        /// Desert terrain
        /// </summary>
        Desert,

        /// <summary>
        /// Jungle terrain
        /// </summary>
        Jungle,

        /// <summary>
        /// Arctic terrain
        /// </summary>
        Artic,

        /// <summary>
        /// Woodland terrain
        /// </summary>
        Woodland,

        /// <summary>
        /// Dryland terrain
        /// </summary>
        Dryland
    }
}
