﻿namespace World.Framework.Terrain
{
    using System.Collections.Generic;

    /// <summary>
    /// Tile map
    /// </summary>
    public class Map
    {
        /// <summary>
        /// Hex neighbor modifiers
        /// </summary>
        private readonly List<HexCoord> directions = new List<HexCoord>()
        {
           new HexCoord(+1, 0), new HexCoord(+1, -1), new HexCoord(0, -1),
           new HexCoord(-1, 0), new HexCoord(-1, +1), new HexCoord(0, +1)
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class.
        /// </summary>
        /// <param name="width">Width of map</param>
        /// <param name="height">Height of map</param>
        public Map(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.Tiles = new Dictionary<HexCoord, Tile>();
        }

        /// <summary>
        /// Gets the width of map
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Gets the height of map
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// Gets or sets water level of map
        /// </summary>
        public float WaterLevel { get; set; }

        /// <summary>
        /// Gets or sets dictionary of tiles that make up the map
        /// </summary>
        public Dictionary<HexCoord, Tile> Tiles { get; set; }

        /// <summary>
        /// Gets neighboring tiles for a given tile
        /// </summary>
        /// <param name="tile">Tile to get neighbors of</param>
        /// <returns>Neighbors of tile</returns>
        public List<Tile> GetNeighbors(Tile tile)
        {
            var ret = new List<Tile>();

            foreach (HexCoord dir in this.directions)
            {
                Tile t = null;
                this.Tiles.TryGetValue(new HexCoord(tile.Coord.Q + dir.Q, tile.Coord.R + dir.R), out t);

                if (t != null)
                {
                    ret.Add(t);
                }
            }

            return ret;
        }
    }
}
