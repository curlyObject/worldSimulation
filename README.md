# World Simulation

## How do I get set up?

* Install git and clone the project
* Install Unity
* Install some IDE that supports C# (Visual Studio 2017 or one of many alternatives)

## How do I run it?

* Start Unity and open the project (the `/World` directory)
* Press the play button to build and run

### Controls

* Move: WASD or arrow keys
* Zoom: Q and E
* Toggle visualisation (moisture is blue and temperature is red): V
* Toggle coordinate labels (o is offset and a is axial): T
* Re-generate regional map: R
* Re-generate world map: G
* Exit: Escape
